package firedancer.plugin.plugin

import org.gradle.api.JavaVersion
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.artifacts.Dependency
import org.gradle.api.artifacts.component.ModuleComponentSelector
import org.gradle.api.artifacts.result.ResolvedDependencyResult
import org.gradle.api.tasks.testing.Test

class FiredancerPluginPlugin implements Plugin<Project> {

    static final GRADLE_PLUGIN_IDS = ['com.gradle.build-scan',
                                      'groovy',
                                      'idea',
                                      'jacoco',
                                      'com.gradle.plugin-publish']

    static final THIRDPARTY_PLUGIN_IDS = ['com.github.kt3k.coveralls']

    static final NEBULA_PLUGIN_IDS = ['nebula.contacts',
                                      'nebula.dependency-lock',
                                      'nebula.facet',
                                      'nebula.info',
                                      'nebula.java-cross-compile',
                                      'nebula.javadoc-jar',
                                      'nebula.maven-apache-license',
                                      'nebula.maven-publish',
                                      'nebula.nebula-release',
                                      'nebula.optional-base',
                                      'nebula.source-jar',
                                      'nebula.integtest'
    ]

    static final PLUGIN_IDS = GRADLE_PLUGIN_IDS + THIRDPARTY_PLUGIN_IDS + NEBULA_PLUGIN_IDS

    @Override
    void apply(Project project) {
        println PLUGIN_IDS
        project.with {
            PLUGIN_IDS.each {
                println it
                plugins.apply(it)
            }

            if (!group) {
                group = 'com.foxfarmroad.firedancer'
            }

            sourceCompatibility = JavaVersion.VERSION_1_8
            targetCompatibility = JavaVersion.VERSION_1_8

            repositories {
                maven { url 'https://plugins.gradle.org/m2/' }
                jcenter()
            }

            dependencies {
                compile gradleApi()

                compile 'com.gradle:build-scan-plugin:1.+'

                testCompile 'com.netflix.nebula:nebula-test:6.+'
            }

            buildScan {
                licenseAgreementUrl = 'https://gradle.com/terms-of-service'
                licenseAgree = 'yes'
                publishAlways()
            }

            jacocoTestReport {
                reports {
                    xml.enabled = true // coveralls plugin depends on xml format report
                    html.enabled = true
                }
            }
    
            tasks.withType(Test) { task ->
                minHeapSize = '32m'
                maxHeapSize = '256m'
                jvmArgs "-XX:MaxPermSize=512m"
                doFirst {
                    // Add the execution data only if the task runs
                    jacocoTestReport.executionData += files("$buildDir/jacoco/${task.name}.exec")
                }
            }

            plugins.withId('com.gradle.plugin-publish') {
                pluginBundle {
                    website = "https://gitlab.com/foxfarmroad/${name}"
                    vcsUrl = "https://gitlab.com/foxfarmroad/${name}.git"

                    description = project.description

                    mavenCoordinates {
                        groupId = project.group
                        artifactId = project.name
                    }

                    tasks.publishPlugins.dependsOn tasks.check

//                    enableResolvedVersionInPluginPortalPom(project)

                    gradle.taskGraph.whenReady { graph ->
                        tasks.publishPlugins.onlyIf {
                            graph.hasTask(':final')
                        }
                    }
                }
            }

//            bintrayUpload.enabled = false
        }
    }

    def enableResolvedVersionInPluginPortalPom(Project project) {
        project.pluginBundle {
            withDependencies { List<Dependency> deps ->
                def resolvedDeps = project.configurations.runtimeClasspath.incoming.resolutionResult.allDependencies
                deps.each { Dependency dep ->
                    String group = dep.groupId
                    String artifact = dep.artifactId
                    ResolvedDependencyResult found = resolvedDeps.find { r ->
                        (r.requested instanceof ModuleComponentSelector) &&
                                (r.requested.group == group) &&
                                (r.requested.module == artifact)
                    }

                    dep.version = found.selected.moduleVersion.version
                }
            }
        }
    }
}