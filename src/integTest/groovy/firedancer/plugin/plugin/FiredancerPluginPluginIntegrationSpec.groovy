package firedancer.plugin.plugin

import nebula.test.IntegrationSpec

class FiredancerPluginPluginIntegrationSpec extends IntegrationSpec {
    def 'plugin applies'() {
        buildFile << """
        apply plugin: 'firedancer.plugin-plugin'
        """

        expect:
        runTasksSuccessfully('help')
    }

    def 'plugin publishing is available'() {
        buildFile << """
        apply plugin: 'firedancer.plugin-plugin'
        
        pluginBundle {
            plugins {
                kotlinPlugin {
                    id = 'firedancer.kotlin'
                    displayName = 'Firedancer Kotlin plugin'
                    description = project.description
                    tags = ['firedancer', 'kotlin']
                }
            }
        }
        """

        expect:
        runTasksSuccessfully('help')
    }
}
