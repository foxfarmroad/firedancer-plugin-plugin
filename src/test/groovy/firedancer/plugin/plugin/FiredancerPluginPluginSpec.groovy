package firedancer.plugin.plugin

import nebula.test.ProjectSpec

class FiredancerPluginPluginSpec extends ProjectSpec {
    def 'apply does not throw exceptions'() {
        when:
        project.apply plugin: 'firedancer.plugin-plugin'

        then:
        noExceptionThrown()
    }

    def 'apply is idempotent'() {
        when:
        project.apply plugin: 'firedancer.plugin-plugin'
        project.apply plugin: 'firedancer.plugin-plugin'

        then:
        noExceptionThrown()
    }
}
